package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.service.ICommandService;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) throws AbstractException {
        if (command == null) throw new CommandNotSupportedException();
        commandRepository.add(command);
    }
    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        return commandRepository.getCommandByName(name);
    }
    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) throws AbstractException {
        if (argument == null || argument.isEmpty()) throw  new ArgumentNotSupportedException();
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandWithArgument() {
        return commandRepository.getCommandWithArgument();
    }

    @Nullable
    @Override
    public  Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
